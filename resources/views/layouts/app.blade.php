<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-light bg-dark">
            <a class="navbar-brand" href="#">Teste Dev</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse justify-content-end " id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="far fa-file-alt"></i> Posts <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{url('posts/create')}}"><i class="far fa-plus-square"></i> Adicionar Post</a>
                            <a class="dropdown-item" href="{{ url('posts')}}"><i class="far fa-file-alt"></i> Posts Salvos</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-user-circle"></i> {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modalMeusDados">Meus Dados</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>

        <div id="modalMeusDados" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Meus Dados</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-meus-dados" style="display:none">

                        </div>
                        <form class="form-meus-dados" method="post">
                            <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" id="user_id" name="user_id" value="{{Auth::id()}}">
                            <input type="hidden" id="input_troca_senha" name="input_troca_senha" value="0">
                            <div class="form-group">
                                <label for="">Nome</label>
                                <input type="text" name="name" class="form-control" id="name" value="{{Auth::user()->name}}">
                            </div>
                            <div class="form-group">
                                <label for="">E-mail</label>
                                <input type="email" name="email" class="form-control" disabled id="email" value="{{Auth::user()->email}}">
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-mostra-senha" name="button">Alterar Senha</button>
                                <button type="button" class="btn btn-secondary btn-not-senha" style="display:none" name="button">Cancelar</button>
                            </div>
                            <div class="troca-senha" style="display:none">
                                <div class="form-group">
                                    <input id="password" type="password" placeholder="Senha" class="form-control" name="password">
                                </div>
                                <div class="form-group ">
                                    <input id="password-confirm" type="password" placeholder="Confirme a senha" class="form-control" name="password_confirmation" >
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success btn-salvar-meus-dados">
                            Salvar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @yield('content')

    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $(document).on('click', '.btn-salvar-meus-dados', function(event) {
                event.preventDefault();
                if($('#input_troca_senha').val() == '1'){
                    if($('#password').val() != '' && $('#name').val() != ''){
                        $.ajax({
                            url: '{{url("meus-dados/update")}}',
                            type: 'post',
                            data: $('.form-meus-dados').serialize()
                        })
                        .done(function(dados) {
                            if(dados.status == '1'){
                                $('.alert-meus-dados').removeClass('alert-danger');
                                $('.alert-meus-dados').addClass('alert-'+dados.tipo);
                                $('.alert-meus-dados').html(dados.menssages);
                                $('.alert-meus-dados').show();
                            }else{
                                $('.alert-meus-dados').html('');
                                $('.alert-meus-dados').addClass('alert-'+dados.tipo);
                                var menssages = dados.menssages.password;
                                for (var i = 0; i < menssages.length; i++) {
                                    $('.alert-meus-dados').append(menssages[i]+'<br>');
                                }
                                $('#password').val('');
                                $('#password-confirm').val('');
                                $('.alert-meus-dados').show();
                            }
                        })
                    } else{
                        alert('Preencha todos os campos');
                    }
                }else{
                    if($('#name').val() != ''){
                        $.ajax({
                            url: '{{url("meus-dados/update")}}',
                            type: 'post',
                            data: $('.form-meus-dados').serialize()
                        })
                        .done(function(dados) {
                            if(dados.status == '1'){
                                $('.alert-meus-dados').removeClass('alert-danger');
                                $('.alert-meus-dados').addClass('alert-'+dados.tipo);
                                $('.alert-meus-dados').html(dados.menssages);
                                $('.alert-meus-dados').show();
                            }else{
                                $('.alert-meus-dados').html('');
                                $('.alert-meus-dados').addClass('alert-'+dados.tipo);
                                var menssages = dados.menssages.password;
                                for (var i = 0; i < menssages.length; i++) {
                                    $('.alert-meus-dados').append(menssages[i]+'<br>');
                                }
                                $('#password').val('');
                                $('#password-confirm').val('');
                                $('.alert-meus-dados').show();
                            }
                        })
                    }else{
                        alert('Preencha todos os campos');
                    }
                }

            });


            $(document).on('click', '.btn-mostra-senha', function(event) {
                $('.troca-senha').show();
                $('#input_troca_senha').val('1');
                $('.btn-not-senha').show();
            });

            $(document).on('click', '.btn-not-senha', function(event) {
                $('#password').val('');
                $('#password-confirm').val('');
                $('#input_troca_senha').val('0');
                $('.btn-not-senha').hide();
                $('.troca-senha').hide();

            });

        });
    </script>
</body>
</html>
