@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if(session("success"))
                    <div class="alert alert-success">
                        {{session("success")}}
                    </div>
                @endif
                <div class="alert al">

                </div>
                <div class="card">
                    <div class="card-header">Posts</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Ações</th>
                                    <th>Titulo</th>
                                    <th>Descrição</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($posts as $post)
                                    <tr>
                                        <td>
                                            <form class="form-list-delete-post" action="{{action('PostController@destroy', $post->id)}}" method="post">
                                                {{csrf_field()}}
                                                <input name="_method" type="hidden" value="DELETE">
                                                <button class="btn btn-danger" title="Deletar" type="submit"><i class="fas fa-trash-alt"></i></button>
                                            </form>
                                            <a href="{{url('posts/'.$post->id)}}" title="Editar" class="btn btn-primary"><i class="far fa-edit"></i></a>

                                        </td>
                                        <td>{{$post->titulo}}</td>
                                        <td>{{$post->descricao}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Request::segment(2) == 'create' || isset($edit))

        <div id="modalFormPost" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form method="post"  action="{{Request::segment(2) == 'create' ? url('posts') : action('PostController@update', $post_edit->id)}}">
                        @if (isset($edit))
                            <input type="hidden" name="_method" value="PATCH">
                        @endif
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">{{Request::segment(2) == 'create' ? 'Cadastro' : 'Editar'}}</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div><br />
                            @endif

                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="">Título</label>
                                <input type="text" name="titulo" class="form-control" value="{{Request::segment(2) == 'create' ? '' : $post_edit->titulo}}">
                            </div>
                            <div class="form-group">
                                <label for="">Descrição</label>
                                <textarea name="descricao" class="form-control" >{{Request::segment(2) == 'create' ? '' : $post_edit->descricao}}</textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">
                                Salvar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#modalFormPost').modal();
                $('#modalFormPost').modal('show');
            });
        </script>
    @endif
@endsection
