<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A senha devem ter pelo menos seis caracteres e ser a mesma da confirmação.',
    'reset' => 'Sua senha foi redefinida!',
    'sent' => 'Enviamos um e-mail com o link de redefinição de senha!',
    'token' => 'Esse token de redefinição de senha é inválida.',
    'user' => "Não podemos encontrar um usuário com esse login.",

];
