<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Usuário e senha inválidos.',
    'throttle' => 'Muitas tentativas de login. Por favor, tente novamente em :seconds segundos.',
    'email' => 'E-mail',
    'username' => 'Usuário',
    'name' => 'Nome completo',
    'password' => 'Senha',
    'cwpassword' => 'Confirme a senha',
    'remember' => 'Guardar o login',
    'btn_login' => 'Entrar',
    'forgot' => 'Esqueceu a senha?',
];
