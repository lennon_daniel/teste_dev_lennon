<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $form = $request->all();
        $user = User::find($request['user_id']);
        if($form['input_troca_senha'] == '1'){
            $validator = Validator::make($form, [
                'name' => 'required|string|max:255',
                'password' => 'required|string|min:6|confirmed',
            ]);

        }else{
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
            ]);
        }


        if($validator->fails()) {
            return response()->json([
                'status' => '0',
                'tipo' => 'danger',
                'menssages' => $validator->messages()
            ]);
        }else{
            if($form['input_troca_senha'] == '1'){
                $user->name = $form['name'];
                $user->password = Hash::make($form['password']);
                $user->save();
            }else{
                $user->name = $form['name'];
                $user->save();
            }
            return response()->json([
                'menssages' => 'Dados Atualizado com sucesso',
                'tipo' => 'success',
                'status' => '1',
            ]);
        }

    }
}
