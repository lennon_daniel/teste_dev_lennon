
A para execução do projeto siga as seguintes etapas

** Atenção os passos a baixo são para execução em ambiente local

1 - Crie um banco de dados com o nome de teste_dev_lennon no seu mysql
2 - Abra o arquivo .env e informe os dados de acesso ao seu banco
3 - Abra o terminal acessa a pasta do projeto e execute os seguntes comandos
4 - php artisan migrate
5 - php artisan serve
6 - Agora abra o navegador com a seguinte url http://127.0.0.1:8000 e aproveita a aplicação

Para alterar a senha acesse o menu com nome do usuário no dropdown que se abrir acesse a opção meus dados
